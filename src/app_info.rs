use tracing::info;

#[derive(Debug)]
#[allow(dead_code)]
pub struct AppInfo {
    pub name: &'static str,
    pub authors: &'static str,
    pub description: &'static str,
    pub repository: &'static str,
    pub version: &'static str,
}

impl Default for AppInfo {
    fn default() -> Self {
        AppInfo {
            name: env!("CARGO_PKG_NAME"),
            authors: env!("CARGO_PKG_AUTHORS"),
            description: env!("CARGO_PKG_DESCRIPTION"),
            repository: env!("CARGO_PKG_REPOSITORY"),
            version: env!("CARGO_PKG_VERSION"),
        }
    }
}

impl AppInfo {
    pub fn print() {
        info!("{:#?}", Self::default());
    }
}
