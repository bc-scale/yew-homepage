use yew_homepage::app_info::AppInfo;
use yew_homepage::app_root::App;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

fn main() {
    console_error_panic_hook::set_once();
    tracing_wasm::set_as_global_default();
    AppInfo::print();
    yew::Renderer::<App>::new().render();
}
